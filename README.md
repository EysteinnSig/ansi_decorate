# ansi_decorate

Small tool to color and highlight text from cin or file.  Works similar to grep.

# Summary

When skimming through large log files for keywords, its easy to loose track. This tiny tool highlight selected words or sentences using simple regex and helps them to stick out for the reader.  
Writing it was an interesting exercise in using ANSI escapes and color codes and capturing cin from pipes. 

Usage is similar to grep (which can do similar thing with a little nudging) so most linux/unix users should be familiar with the syntax / arguments.

# Example of use:

![alt text](https://gitlab.com/EysteinnSig/ansi_decorate/raw/main/example/deco_example.png)

# Compilation and installation

To compile with cmake:

```
mkdir build && cd build && cmake ../ && make
```


To install:

```
sudo make install
```


