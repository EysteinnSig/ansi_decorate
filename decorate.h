#pragma once

#include <string>
#include <map>
#include <regex>

namespace decorate {

std::map<std::string, std::string> colors;
const std::string ansi_bg_prefix = "\033[48;2;";
const std::string ansi_fg_prefix = "\033[38;2;";
const std::string ansi_blink = "\033[5m";
const std::string ansi_underline = "\033[4m";
const std::string ansi_reset = "\x1b[0m";


inline const std::string ansi_color(uint8_t r, uint8_t g, uint8_t b) {
    static char buffer[30];
    int len = sprintf(buffer, "%i;%i;%im", r, g, b);
    return std::string(buffer, len);
}

void fill_colors_dict()
{
    colors.insert(std::make_pair("red", ansi_color(222,56,43)));
    colors.insert(std::make_pair("green", ansi_color(57, 181, 74)));
    colors.insert(std::make_pair("yellow", ansi_color(255, 199, 6)));
    colors.insert(std::make_pair("blue", ansi_color(0, 111, 184)));
    colors.insert(std::make_pair("magenta", ansi_color(118, 38, 113)));
    colors.insert(std::make_pair("cyan", ansi_color(44, 181, 233)));
    colors.insert(std::make_pair("white", ansi_color(204, 204, 204)));
    colors.insert(std::make_pair("bblack", ansi_color(128, 128, 128)));
    colors.insert(std::make_pair("bred", ansi_color(255, 0, 0)));
    colors.insert(std::make_pair("bgreen", ansi_color(0, 255, 0)));
    colors.insert(std::make_pair("byellow", ansi_color(255, 255, 0)));
    colors.insert(std::make_pair("bblue", ansi_color(0, 0, 255)));
    colors.insert(std::make_pair("bmagenta", ansi_color(255, 0, 255)));
    colors.insert(std::make_pair("bcyan", ansi_color(0, 255, 255)));
    colors.insert(std::make_pair("bwhite", ansi_color(255, 255, 255)));
}


std::string color2ansi(const std::string &text)
{
    auto iter = colors.find(text);
    if (iter != colors.end())
    {
        return iter->second;
    }
    return "";
}


std::string build_ansi(std::string fg_col, std::string bg_col, bool blink = false, bool underline = false)
{
    std::string ansicmd = "";
    if (!fg_col.empty())
        ansicmd += ansi_fg_prefix + fg_col;
    if (!bg_col.empty())
        ansicmd += ansi_bg_prefix + bg_col;
    
    ansicmd += blink?ansi_blink:""; //"\033[5m":"";
    ansicmd += underline?ansi_underline:""; //"\033[4m":"";
    return ansicmd;
}

void process_stream(std::istream *stream, const std::string &att_str, const std::regex &pattern)
{
    std::string prev;
    const int buffersize = 1024;
    
    char buffer[buffersize];

    while (true) 
    {
        stream->read(buffer, buffersize);

        int gcnt = stream->gcount();
        if (!gcnt)
            break;
        std::string s(buffer, gcnt);
        std::string combined = prev+s;
        
        int accu = 0;
        for(std::sregex_iterator i = std::sregex_iterator(combined.begin(), combined.end(), pattern);
                                i != std::sregex_iterator();
                                ++i )
        {
            std::smatch m = *i;
            
            if (m.position()+accu < prev.size())
            {
                prev.insert(m.position()+accu, att_str);
                accu = accu + att_str.size();
            } else 
            {
                int pos = m.position()+accu - prev.size();
                s.insert(pos, att_str);
                accu = accu + att_str.size();
            }

            if (m.position()+accu+m.length() < prev.size())
            {
                prev.insert(m.position()+accu+m.length(), ansi_reset);
                accu = accu + ansi_reset.size();
            }
            else
            {
                int pos = m.position()+accu+m.length() - prev.size();
                s.insert(pos, ansi_reset);
                accu = accu + ansi_reset.size();
            }
        }
        std::cout << prev;
        prev = s;

    }
    std::cout << prev;
}

void die()
{
    exit(1);
}

/*
 Convert fg_color:bg_color+effects to ansi sequence.
 */
std::string ansify_decorator_str(std::string arg)
{
    //printf("ARGUMENTS: %s\n", arg.c_str());
    int colon_idx = arg.find_first_of(':');
    int plus_idx = arg.find_first_of('+');
    std::string fg_col = "red";
    std::string bg_col = "";
    bool blink = false;
    bool underline = false;
    std::string options = "";


    int idx;
    idx = arg.find_first_of("+:");
    if (idx == std::string::npos)
        fg_col = arg;
    else
        fg_col = arg.substr(0, idx);
    

    // if background color is defined
    if (colon_idx != std::string::npos)
    {
        if (plus_idx == std::string::npos)
            bg_col = arg.substr(colon_idx+1);
        else
            bg_col = arg.substr(colon_idx+1, plus_idx-colon_idx -1);

    }

    // options found
    if (arg.find('+') != std::string::npos)
    {
        options = arg.substr(arg.find('+'));
        
        blink = (options.find("+blink") != std::string::npos) || (options.find("+b") != std::string::npos);
        underline = (options.find("+underline") != std::string::npos) || (options.find("+u") != std::string::npos);
    }
    return build_ansi(color2ansi(fg_col), color2ansi(bg_col), blink, underline);
}


}