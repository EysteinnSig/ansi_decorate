cmake_minimum_required(VERSION 3.0.0)
project(deco VERSION 0.1.0)

include(CTest)
enable_testing()

add_executable(deco main.cpp)
install(TARGETS deco DESTINATION bin)

set(CPACK_PROJECT_NAME ${PROJECT_NAME})
set(CPACK_PROJECT_VERSION ${PROJECT_VERSION})

target_compile_features(deco PRIVATE cxx_std_17)



include(CPack)
