#include <iostream>
#include <string>
#include <vector>
#include <cassert>
#include <unistd.h>    /* for getopt */
#include <fstream>

#include "decorate.h"


void print_help(std::ostream &stream)
{
    stream << "Usage: deco [OPTION]... PATTERNS [FILE]..." << std::endl;
    stream << "Decorate PATTERNS in each FILE." << std::endl;
    stream << "Example: deco -i -d red:cyan+blink+u 'hello world' menu.h main.c" << std::endl;
    stream << "Example: deco -i -d :cyan+u 'hello world' menu.h main.c" << std::endl;
    stream << "Accepts piped input." << std::endl;
    stream << std::endl << "Options:" << std::endl;
    stream << "  -l         show all available colors and then exit." << std::endl;
    stream << "  -i         ignore case in pattern." << std::endl;
    stream << "  -d         set decorator using <fg colro>:<bg color>+<opt>+<opt>. Default: 'red'" << std::endl;
    stream << "             <fg color> and <bg color> are foreground and background colors, <opt> are additional options" << std::endl;
    stream << "             that includes 'blink' and 'underline' (can be shortened to 'b' and 'u')" << std::endl;
}

void list_decorators()
{
    std::cout << "Available colors:" << std::endl;
    std::cout << "      Foreground          Background" << std::endl;
    for (auto iter = decorate::colors.begin(); iter != decorate::colors.end(); iter++)
    {
        //std::cout << decorate::build_ansi("red", "", false, false);
        
        std::cout << "        " << decorate::ansi_fg_prefix+decorate::color2ansi(iter->first);
        std::cout << iter->first << decorate::ansi_reset;
        std::cout << std::string(20-iter->first.size(), ' ');
        std::cout << decorate::ansi_bg_prefix+decorate::color2ansi(iter->first);
        std::cout << iter->first << decorate::ansi_reset << std::endl;
    }
    exit(0);
}

int main(int argc, char** argv)
{
    bool case_insensitive = false;
    std::string decorator_arg = "red";
    std::string pattern_arg = "";
    std::vector<std::string> filenames_arg;

    decorate::fill_colors_dict();

    int c;
    while ((c = getopt(argc, argv, ":lihd:")) != -1)
    {
        switch (c) {
            case 'l':
                list_decorators();
                break;
            case 'i': //printf("I set\t%i\n", optind);
                case_insensitive = true;
                break;
            case 'd':
                decorator_arg = optarg;
                break;
            case 'h':
                print_help(std::cout);
                exit(0);
            case '?':
                std::cerr << "decorate: unknown option: " << (char)optopt << std::endl;
                print_help(std::cerr);
                exit(1);
            case ':':
                //printf("Missing arg for %c\n", optopt);
                std::cerr << "decorate: missing arg for: " << (char)optopt << std::endl;
                print_help(std::cerr);
                exit(1);
            default:
                exit(1);
        }
    }

    if (argc == optind)
    {
        std::cerr << "Missing pattern." << std::endl;
        print_help(std::cerr);
        exit(1);
    }
    pattern_arg = argv[optind];
        
    for (int k= optind+1; k<argc; k++)
    {
        filenames_arg.push_back(argv[k]);
    }

    std::string ansi_decorator = decorate::ansify_decorator_str(decorator_arg);

    std::regex pattern;
    if (case_insensitive)
    {
        pattern = std::regex(pattern_arg, std::regex::icase);
    }
    else
        pattern = std::regex(pattern_arg);

    if (!filenames_arg.empty())
    {
        for (std::string filename_arg : filenames_arg)
        {
            std::ifstream file;
            file.open(filename_arg);
            if (!file.is_open())
            {
                std::cerr << "decorate: " << filename_arg << ": error opening file" << std::endl;
                continue;
            }
            decorate::process_stream(&file, ansi_decorator, pattern);
        }
    }
    else
        decorate::process_stream(&std::cin, ansi_decorator, pattern);
}